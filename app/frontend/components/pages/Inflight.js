import React from "react";
import { connect } from "react-redux";
// import SwamInfo from "../organisms/SwamInfo";
// import FlightInfo from "../organisms/FlightInfo";
type Prop = {};

class Inflight extends React.Component<Props> {
  render() {
    const { inFlight } = this.props;

    return (
      <div className={"outboundcontainer"}>
        <div className={"outbound__iamgeContainer"}>
          <img
            className={"outbound__image"}
            src={__dirname + "./static/images/inflight.png"}
            alt={"./"}
          />
        </div>
        <div className={"swamInfoContainer"}>
          <div className={"swamInfo_title"}>InFLight</div>
          <div className={"truckContainer"} />

          <div className={"truck__table"}>
            <div className={"truck__truck"}> dfsd</div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { inFlight: state.inFlight_red };
};

export default connect(mapStateToProps)(Inflight);
