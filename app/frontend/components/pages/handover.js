import React from "react";
import Galley from "../modular/Galley";

const galleys = [
  { name: "Galley 1A Loaded 6/6" },
  { name: "Galley 1B Loaded 4/4" },
  { name: "Galley 2A Loaded 8/8" },
  { name: "Galley 2B Loaded 2/2" }
];

export default class Handover extends React.Component {
  render() {
    return (
      <React.Fragment>
        <div className={"handover"}>
          {galleys.map((labels, index) => {
            return <Galley key={index} label={labels.name} />;
          })}
          <div
            className={"handover_button"}
            onClick={() => {
              this.props.history.push("/inflight");
            }}
          >
            Accept Handover
          </div>
        </div>
      </React.Fragment>
    );
  }
}
