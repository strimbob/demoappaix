/* @flow */
import React, { Component } from "react";
import Outbound from "./Outbound.js";
import { connect } from "react-redux";
import { onRoute, arrived } from "../../../redux/2_action/outbound";
class Home extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Outbound />
        <button
          onClick={e => {
            this.props.dispatch(onRoute());
          }}
        >
          on route{" "}
        </button>
        <button
          onClick={e => {
            this.props.dispatch(arrived());
          }}
        >
          on route{" "}
        </button>
        <button
          onClick={e => {
            this.props.history.push("/handover");
          }}
        >
          handover{" "}
        </button>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

export default connect(mapStateToProps)(Home);
