import React from "react";
import { connect } from "react-redux";
import SwamInfo from "../organisms/SwamInfo";
import FlightInfo from "../organisms/FlightInfo";
type Prop = {};

class Outbound extends React.Component<Props> {
  render() {
    const { outbound, flight } = this.props;

    return (
      <div className={"outboundcontainer"}>
        <div className={"outbound__iamgeContainer"}>
          <img
            className={"outbound__image"}
            src={outbound.getImage()}
            alt={"./"}
          />
        </div>
        <SwamInfo outbound={outbound} />
        <FlightInfo flight={flight} />
      </div>
    );
  }
}
// <img />
const mapStateToProps = state => {
  return {
    outbound: state.outbound_red,
    flight: state.flight_red
  };
};
export default connect(mapStateToProps)(Outbound);
