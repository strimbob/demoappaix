import React from "react";
import { FlightInterface } from "../../../redux/models/FlightModel.js";
type Prop = {
  flight: FlightInterface
};

export default class FlightInfo extends React.Component<Props> {
  render() {
    const { flight } = this.props;

    const infoTable = (label: string, number: number | string) => {
      return (
        <React.Fragment>
          <div className={"flight__info"}>
            <div className={"flight__label"}>{label} </div>
            <div className={"flight__number"}>{number} </div>
          </div>
        </React.Fragment>
      );
    };
    return (
      <div className={"flightContainer"}>
        <div className={"flight__top"}>
          <div className={"flight__from"}> {flight.getFrom()} </div>
          <img className={"flight__image"} src={"./static/images/plane.png"} />
          <div className={"flight__to"}> {flight.getTo()} </div>
        </div>
        <div className={"flight__line"}> </div>
        <div className={"flight__bottom"}>
          {infoTable("FLIGHT", flight.getFlightNumber())}
          {infoTable("A/C Stand", flight.getStand())}
        </div>
      </div>
    );
  }
}
