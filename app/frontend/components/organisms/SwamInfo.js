import React from "react";
import { TruckInterface } from "../../../redux/models/TrucksModel.js";
import { OutboundInterface } from "../../../redux/models/OutboundModel.js";
type Prop = {
  outbound: OutboundInterface
};

export default class SwamInfo extends React.Component<Props> {
  render() {
    const { title, truck, outbound } = this.props;
    return (
      <div className={"swamInfoContainer"}>
        <div className={"swamInfo_title"}>{outbound.getTitle()}</div>
        <div className={"truckContainer"}>
          {outbound.getTrucks().map((_truck, index) => {
            return (
              <React.Fragment key={index}>
                <div className={"truck__table"}>
                  <div className={"truck__truck"}> {_truck.getName()}</div>
                  <div className={"truck__status"}> {_truck.getStatus()}</div>
                </div>
              </React.Fragment>
            );
          })}
        </div>
      </div>
    );
  }
}
