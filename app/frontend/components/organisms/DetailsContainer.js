/* @flow */
import React, { Component } from "react";
import ViewSlide from "../modular/ViewSlide.js";
import { StaggeredMotion, spring } from "react-motion";

export default class DetailsContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      activeSlideIndex: 0,
      slides: [
        {
          triggers: 1,
          y: 0,
          content: null
        },
        {
          triggers: 0,
          y: 100,
          content: null
        }
      ]
    };
  }
  setActiveSlide(slideIndex) {
    this.setState({ activeSlideIndex: slideIndex });
  }
  render() {
    const slides = this.state.slides;
    return (
      <div className={"detailsContainer"}>
        <div className={"details__Bottom"}>
          <StaggeredMotion
            defaultStyles={slides}
            styles={prevInterpolatedStyles =>
              prevInterpolatedStyles.map((_, i) => {
                return i === 0
                  ? { y: -(this.state.activeSlideIndex * 100) }
                  : { y: prevInterpolatedStyles[i - 1].y + 100 };
              })
            }
          >
            {interpolatingStyles => (
              <div className="ViewSlider">
                {interpolatingStyles.map((style, i) => {
                  const active = i == this.state.activeSlideIndex,
                    slide = this.state.slides[i];
                  const { activeSlideIndex } = this.state;

                  return (
                    <React.Fragment key={i}>
                      <ViewSlide
                        style={{ top: `${style.y}%` }}
                        active={active}
                        key={i}
                        onClick={
                          activeSlideIndex
                            ? this.setActiveSlide.bind(this, slide.triggers)
                            : () => {}
                        }
                      >
                        <h1>{slide.content}</h1>
                        <div
                          className={"footer"}
                          onClick={
                            activeSlideIndex
                              ? () => {}
                              : this.setActiveSlide.bind(this, slide.triggers)
                          }
                        >
                          <div className={"footer__line"} />
                          <div className={"footer__label"}>
                            Catering Equipment
                          </div>
                        </div>
                      </ViewSlide>
                    </React.Fragment>
                  );
                })}
              </div>
            )}
          </StaggeredMotion>
        </div>
      </div>
    );
  }
}
