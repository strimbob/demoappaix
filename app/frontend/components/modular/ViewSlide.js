import React, { Component } from "react";

export default class ViewSlide extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const isActive = this.props.active ? "active" : "";
    const classes = "ViewSlide " + isActive;
    return (
      <div
        style={this.props.style}
        className={classes}
        onClick={this.props.onClick}
      >
        {this.props.children}
      </div>
    );
  }
}
