import React from "react";

export default class Galley extends React.Component {
  render() {
    const { label } = this.props;
    return (
      <div className={"galleyContainer"}>
        <div className={"galley__label"}>{label}</div>
        <img
          className={"galley__image"}
          src={__dirname + "./static/images/tick.jpg"}
        />
      </div>
    );
  }
}
