import React from "react"
import {Helmet} from "react-helmet";
import header from '../../../redux/data/header.js'
import fonts from '../../../redux/data/fonts.js'

const Headers = () =>
<React.Fragment>
  <Helmet>
  <meta charSet="utf-8" />
  <title>{header.title}</title>
  <meta name="description" content={header.description}></meta>
  <meta name="theme-color" content={header.themeColor}></meta>
  <meta property="og:title" content={header.titleOg}></meta>
  <meta property="og:description" content={header.description}></meta>
  <meta property="og:image" content={header.image}></meta>
  <meta property="og:url" content={header.url}></meta>
  <meta name="mobile-web-app-capable" content="yes"></meta>
  <meta name="apple-mobile-web-app-capable" content="yes"></meta>
  <link rel="canonical" href={header.url} />
  <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
  <link href='https://fonts.gstatic.com' rel='preconnect' crossorigin/>
  <link href={"https://fonts.googleapis.com/css?family="+fonts.comfortaa} rel='stylesheet'/>
  <noscript>Your browser does not support JavaScript </noscript>
  </Helmet>
 </React.Fragment>;

export default Headers
