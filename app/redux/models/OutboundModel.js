//@flow
import { Record } from "immutable";

import TrucksModel, { TruckInterface } from "./TrucksModel.js";

export interface OutboundInterface {
  getTitle(): string;
  setTitle(title: string): OutboundInterface;
  getTrucks(): Array<TruckInterface> | null;
  setTrucks(trucks: Array<TruckInterface>): OutboundInterface;

  getImage(): string | null;
  setImage(path: string): OutboundInterface;
}

export type OutboundType = {
  title: string,
  trucks: Array<TruckInterface> | null,
  path: string | null
};
const defaultOutbound: OutboundType = {
  title: "Ready for OutBound",
  trucks: null,
  path: null
};

const OutboundRecord = Record(defaultOutbound);

export default class OutboundModel extends OutboundRecord<OutboundType>
  implements OutboundInterface {
  getTitle() {
    return this.get("title");
  }

  setTitle(title: string) {
    return this.set("title", title);
  }
  getTrucks() {
    return this.get("trucks");
  }
  setTrucks(trucks: Array<TruckInterface>) {
    return this.set("trucks", trucks);
  }

  setImage(path: string) {
    return this.set("path", path);
  }
  getImage() {
    return this.get("path");
  }
}
