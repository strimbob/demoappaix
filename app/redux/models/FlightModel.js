//@flow
import { Record } from "immutable";

export interface FlightInterface {
  setFrom(from: string): FlightInterface;
  getFrom(): string | null;

  setTo(to: string): FlightInterface;
  getTo(): string | null;

  setFlightNumber(flightNumber: string): FlightInterface;
  getFlightNumber(): string | null;

  getStand(): number | null;
  setStand(stand: number): FlightInterface;

  getGate(): number | null;
  setGate(gate: number): FlightInterface;

  getTermial(): number | null;
  setTermial(termial: number): FlightInterface;
}

export type FlightType = {
  from: string | null,
  to: string | null,
  flightNumber: string | null,
  stand: number | null,
  gate: number | null,
  termial: number | null
};
const defaultFlight: FlightType = {
  from: null,
  to: null,
  flightNumber: null,
  stand: null,
  gate: null,
  termial: null
};

const FlightRecord = Record(defaultFlight);

export default class TrucksModel extends FlightRecord<FlightType>
  implements FlightInterface {
  setFrom(from: string) {
    return this.set("from", from);
  }
  getFrom() {
    return this.get("from");
  }

  setTo(to: string) {
    return this.set("to", to);
  }
  getTo() {
    return this.get("to");
  }

  setFlightNumber(flightNumber: string) {
    return this.set("flightNumber", flightNumber);
  }
  getFlightNumber() {
    return this.get("flightNumber");
  }

  getStand() {
    return this.get("stand");
  }

  setStand(stand: number) {
    return this.set("stand", stand);
  }

  getGate() {
    return this.get("gate");
  }
  setGate(gate: number) {
    return this.set("gate", gate);
  }

  getTermial() {
    return this.get("termial");
  }
  setTermial(termial: number) {
    return this.set("termial", termial);
  }
}
