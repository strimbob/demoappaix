//@flow
import { Record } from "immutable";
export interface TruckInterface {
  setName(name: string): TruckInterface;
  getName(): string | null;
  setStatus(status: string): TruckInterface;
  getStatus(): string | null;
}

export type TruckType = {
  name: null | string,
  status: null | string
};
const defaultTruck: TruckType = {
  name: null,
  status: null
};

const TrucRecord = Record(defaultTruck);

export default class TrucksModel extends TrucRecord<TruckType>
  implements TruckInterface {
  getName() {
    return this.get("name");
  }
  setName(name: string) {
    return this.set("name", name);
  }
  setStatus(status: string) {
    return this.set("status", status);
  }
  getStatus() {
    return this.get("status");
  }
}
