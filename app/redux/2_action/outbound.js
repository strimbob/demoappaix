import Outbound from "../models/OutboundModel.js";
import Trucks from "../models/TrucksModel.js";

export const onRoute = () => (dispatch, getState) => {
  const trucks = new Array(2)
    .fill()
    .map((f, index) =>
      new Trucks()
        .setName("truck" + index)
        .setStatus(`ETA ${21 + index} minutes`)
    );

  const outbound = new Outbound()
    .setTitle("Catering equipment en route")
    .setTrucks(trucks)
    .setImage(__dirname + "./static/images/outboundTruck.png");
  dispatch({ type: "UPDATE_OUTBOUND", payload: outbound });
};

export const arrived = () => (dispatch, getState) => {
  const trucks = new Array(2)
    .fill()
    .map((f, index) =>
      new Trucks()
        .setName("truck" + index)
        .setStatus(`Arrived ${2 - index} minutes ago`)
    );

  const outbound = new Outbound()
    .setTitle("Arrived at aircarft")
    .setTrucks(trucks)
    .setImage(__dirname + "./static/images/arrivedTruck.png");
  dispatch({ type: "UPDATE_OUTBOUND", payload: outbound });
};
