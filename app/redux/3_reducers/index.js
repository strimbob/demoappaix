import { combineReducers } from "redux";
import mqtt_reducer from "./mqtt_reducer";
import outbound_red from "./outbound_red";
import flight_red from "./flight_red.js";
export default combineReducers({
  mqtt_reducer,
  outbound_red,
  flight_red
});
