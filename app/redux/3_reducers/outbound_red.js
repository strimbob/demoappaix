//@flow

import OutboundModel, { OutboundInterface } from "../models/OutboundModel.js";
import TrucksModel from "../models/TrucksModel.js";

const trucks = new Array(2)
  .fill()
  .map((a, index) =>
    new TrucksModel().setName(`truck ${index}`).setStatus(`Ready for OutBound`)
  );

type State = OutboundInterface;

const defaultOutbound = new OutboundModel()
  .setTrucks(trucks)
  .setImage(__dirname + "./static/images/outboundTruck.png");
const outbound_red = (
  state: State = defaultOutbound,
  action: Object
): outbound_red => {
  switch (action.type) {
    case "UPDATE_OUTBOUND": {
      return action.payload;
    }
    default: {
      break;
    }
  }
  return state;
};
export default outbound_red;
