//@flow

import FlightModel, { FlightInterface } from "../models/FlightModel.js";

const flight = new FlightModel()
  .setTo("HAM")
  .setFrom("AMS")
  .setFlightNumber("DL8484")
  .setStand(9);

type State = FlightInterface;

const outbound_red = (state: State = flight, action: Object): outbound_red => {
  switch (action.type) {
    case "SET_FLIGHT": {
      return { ...state, notification: action.payload };
    }
    default: {
      break;
    }
  }
  return state;
};
export default outbound_red;
