import { Notification } from "../models/Notification.js";

const mqtt_reducer = (
  state = new Notification(),
  action: Object
): mqtt_reducer => {
  switch (action.type) {
    case "Notification": {
      return { ...state, notification: action.payload };
    }
    default: {
      break;
    }
  }

  return state;
};
export default mqtt_reducer;
