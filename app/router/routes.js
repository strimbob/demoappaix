/* @flow */
import Home from "../frontend/components/pages/home.js";
import Handover from "../frontend/components/pages/handover";
import Inflight from "../frontend/components/pages/Inflight.js";
import AppRoot from "./app-root";

const routes = [
  {
    component: AppRoot,
    routes: [
      { path: "/", exact: true, component: Home },
      { path: "/handover", exact: true, component: Handover },
      { path: "/inflight", exact: true, component: Inflight }
    ]
  }
];
export default routes;
