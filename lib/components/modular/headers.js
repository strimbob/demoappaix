"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _reactHelmet = require("react-helmet");

var _header = require("../../../redux/data/header.js");

var _header2 = _interopRequireDefault(_header);

var _fonts = require("../../../redux/data/fonts.js");

var _fonts2 = _interopRequireDefault(_fonts);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Headers = function Headers() {
  return _react2.default.createElement(
    _react2.default.Fragment,
    null,
    _react2.default.createElement(
      _reactHelmet.Helmet,
      null,
      _react2.default.createElement("meta", { charSet: "utf-8" }),
      _react2.default.createElement(
        "title",
        null,
        _header2.default.title
      ),
      _react2.default.createElement("meta", { name: "description", content: _header2.default.description }),
      _react2.default.createElement("meta", { name: "theme-color", content: _header2.default.themeColor }),
      _react2.default.createElement("meta", { property: "og:title", content: _header2.default.titleOg }),
      _react2.default.createElement("meta", { property: "og:description", content: _header2.default.description }),
      _react2.default.createElement("meta", { property: "og:image", content: _header2.default.image }),
      _react2.default.createElement("meta", { property: "og:url", content: _header2.default.url }),
      _react2.default.createElement("meta", { name: "mobile-web-app-capable", content: "yes" }),
      _react2.default.createElement("meta", { name: "apple-mobile-web-app-capable", content: "yes" }),
      _react2.default.createElement("link", { rel: "canonical", href: _header2.default.url }),
      _react2.default.createElement("meta", { name: "viewport", content: "width=device-width, initial-scale=1" }),
      _react2.default.createElement("link", { href: "https://fonts.gstatic.com", rel: "preconnect", crossorigin: true }),
      _react2.default.createElement("link", { href: "https://fonts.googleapis.com/css?family=" + _fonts2.default.comfortaa, rel: "stylesheet" }),
      _react2.default.createElement(
        "noscript",
        null,
        "Your browser does not support JavaScript "
      )
    )
  );
};

var _default = Headers;
exports.default = _default;
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(Headers, "Headers", "app/frontend/components/modular/headers.js");

  __REACT_HOT_LOADER__.register(_default, "default", "app/frontend/components/modular/headers.js");
}();

;