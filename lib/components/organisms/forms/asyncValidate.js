'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var sleep = function sleep(ms) {
  return new Promise(function (resolve) {
    return setTimeout(resolve, ms);
  });
};

var asyncValidate = function asyncValidate(values /*, dispatch */) {
  return sleep(1000).then(function () {
    // simulate server latency
    if (['foo@foo.com', 'bar@bar.com'].includes(values.email)) {
      // eslint-disable-next-line no-throw-literal
      throw { email: 'Email already Exists' };
    }
  });
};

var _default = asyncValidate;
exports.default = _default;
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(sleep, 'sleep', 'app/frontend/components/organisms/forms/asyncValidate.js');

  __REACT_HOT_LOADER__.register(asyncValidate, 'asyncValidate', 'app/frontend/components/organisms/forms/asyncValidate.js');

  __REACT_HOT_LOADER__.register(_default, 'default', 'app/frontend/components/organisms/forms/asyncValidate.js');
}();

;