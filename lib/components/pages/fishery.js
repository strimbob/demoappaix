'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _dec, _class;

var _react = require('react');

var React = _interopRequireWildcard(_react);

var _reactResponsiveRedux = require('react-responsive-redux');

var _reactRedux = require('react-redux');

var _indexPage = require('../../../redux/data/indexPage.js');

var _remember = require('../../../redux/2_action/remember.js');

var _props_type = require('../../types/props_type.js');

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Fishery = (_dec = (0, _reactRedux.connect)(function (store) {
  return {
    remember: store.remember,
    fishery: store.fishery
  };
}), _dec(_class = function (_React$Component) {
  _inherits(Fishery, _React$Component);

  function Fishery(props) {
    _classCallCheck(this, Fishery);

    return _possibleConstructorReturn(this, (Fishery.__proto__ || Object.getPrototypeOf(Fishery)).call(this, props));
  }

  _createClass(Fishery, [{
    key: 'componentWillMount',
    value: function componentWillMount() {}
  }, {
    key: 'render',
    value: function render() {

      if (this.props.fishery.fetched) {
        return React.createElement(
          'div',
          null,
          React.createElement(
            'h1',
            null,
            this.props.fishery.story.fishery,
            ' '
          ),
          ' '
        );
      } else {
        return React.createElement(
          'div',
          null,
          React.createElement(
            'h1',
            null,
            yes
          ),
          React.createElement(
            _reactResponsiveRedux.PhoneScreen,
            null,
            React.createElement(
              'div',
              null,
              React.createElement(
                'h1',
                null,
                "You are a PhoneScreen device hi it is STILL fgfg  asd working! now way and t fast!" + this.props.remember.rembered
              )
            )
          ),
          React.createElement(
            _reactResponsiveRedux.TabletScreen,
            null,
            React.createElement(
              'div',
              null,
              React.createElement(
                'h1',
                null,
                " You are a TabletScreen device" + this.props.remember.rembered,
                ' '
              )
            )
          ),
          React.createElement(
            _reactResponsiveRedux.DesktopScreen,
            null,
            React.createElement(
              'div',
              null,
              React.createElement(
                'h1',
                null,
                "You are a DesktopScreen device" + this.props.remember.rembered
              )
            )
          )
        );
      }
    }
  }]);

  return Fishery;
}(React.Component)) || _class);
exports.default = Fishery;
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(Fishery, 'Fishery', 'app/frontend/components/pages/fishery.js');
}();

;