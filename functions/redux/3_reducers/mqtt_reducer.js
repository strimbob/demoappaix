"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _Notification = require("../models/Notification.js");

var mqtt_reducer = function mqtt_reducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : new _Notification.Notification();
  var action = arguments[1];

  switch (action.type) {
    case "Notification":
      {
        return _extends({}, state, { notification: action.payload });
      }
    default:
      {
        break;
      }
  }

  return state;
};
var _default = mqtt_reducer;
exports.default = _default;
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(mqtt_reducer, "mqtt_reducer", "app/redux/3_reducers/mqtt_reducer.js");

  __REACT_HOT_LOADER__.register(_default, "default", "app/redux/3_reducers/mqtt_reducer.js");
}();

;