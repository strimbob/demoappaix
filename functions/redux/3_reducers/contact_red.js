"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.default = reducer;
var _fishery = [];
var _fisherySelector = [];
function reducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    openPopUp: false,
    closePopUP: true,
    messageSend: false,
    joinUP: false,
    showPOPUP: false,
    popUpOpen: false,
    messageSendContact: false,
    inputTitle: ""
  };
  var action = arguments[1];


  switch (action.type) {
    case "EMAILMESSAGE_CONTACT":
      {
        return _extends({}, state, { messageSendContact: true });
      }
    case "EMAILMESSAGEBACKTOFORMCONTACT":
      {
        return _extends({}, state, { messageSendContact: false });
      }
    case "POPUPOPEN":
      {
        console.log("new pop up open iris");
        return _extends({}, state, { popUpOpen: true });
      }

    case "POPUPCLOSE":
      {
        console.log("new pop up close iris");
        return _extends({}, state, { popUpOpen: false });
      }

    case "OPENPOPUP":
      {
        return _extends({}, state, { openPopUp: true, closePopUP: false });
      }
    case "CLOSEPOPUP":
      {
        return _extends({}, state, { openPopUp: false, closePopUP: true });
      }
    case "EMAILMESSAGE":
      {
        return _extends({}, state, { messageSend: true });
      }
    case "COOKIES-HAVE-JIONED":
      {}
    case "EMAILMESSAGEBACKTOFORM":
      {
        return _extends({}, state, { messageSend: false });
      }
    case "POPUPCHANGEBUTTON":
      {
        return _extends({}, state, { inputTitle: action.payload });
      }
    case "POPUPJOINUP":
      {
        return _extends({}, state, { joinUP: true });
      }
    case "POPUPSHOW":
      {
        return _extends({}, state, { showPOPUP: true });
      }

  }
  return state;
}
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(_fishery, "_fishery", "app/redux/3_reducers/contact_red.js");

  __REACT_HOT_LOADER__.register(_fisherySelector, "_fisherySelector", "app/redux/3_reducers/contact_red.js");

  __REACT_HOT_LOADER__.register(reducer, "reducer", "app/redux/3_reducers/contact_red.js");
}();

;