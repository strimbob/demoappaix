"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getChild = exports.put = undefined;

var _firebase = require("firebase");

var _firebase2 = _interopRequireDefault(_firebase);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var put = exports.put = function put(path, data) {
  _firebase2.default.database().ref(path).push(data);
};
var getChild = exports.getChild = function getChild(parent) {
  var ref = _firebase2.default.database().ref(parent);
  return ref.once("value");
};
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(put, "put", "app/redux/firebase/fireBaseReqest.js");

  __REACT_HOT_LOADER__.register(getChild, "getChild", "app/redux/firebase/fireBaseReqest.js");
}();

;