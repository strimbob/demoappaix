"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.emailMessageBacktoFormContact = emailMessageBacktoFormContact;
exports.popUpOpen = popUpOpen;
exports.popUpClose = popUpClose;
exports.emailMessage = emailMessage;
exports.asked = asked;
exports.emailMessageBacktoForm = emailMessageBacktoForm;
exports.popUpChangeButton = popUpChangeButton;
exports.downloadRequst = downloadRequst;
exports.joinUp = joinUp;
exports.openPopUP = openPopUP;
exports.closePopUP = closePopUP;

var _jsCookie = require("js-cookie");

var _jsCookie2 = _interopRequireDefault(_jsCookie);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function emailMessageBacktoFormContact() {
    return function (dispatch) {
        dispatch({ type: "EMAILMESSAGEBACKTOFORMCONTACT" });
    };
}

function popUpOpen() {
    return function (dispatch) {
        dispatch({ type: "POPUPOPEN" });
    };
}
function popUpClose() {
    return function (dispatch) {
        dispatch({ type: "POPUPCLOSE" });
    };
}
function emailMessage(data) {
    return function (dispatch) {
        sendDataToSever("sendContactEmail", data);
        dispatch({ type: "EMAILMESSAGE_CONTACT" });
    };
}

function asked() {
    return function (dispatch) {
        dispatch({ type: "POPUPJOINUP" });
    };
}

function emailMessageBacktoForm() {
    return function (dispatch) {
        dispatch({ type: "EMAILMESSAGEBACKTOFORM" });
    };
}

function popUpChangeButton(data) {
    return function (dispatch) {
        dispatch({ type: "POPUPCHANGEBUTTON", payload: data });
    };
}

function downloadRequst(data) {
    return function (dispatch) {
        console.log(data);
        sendDataToSever("download", data);
        dispatch({ type: "POPUPJOINUP" });
        dispatch({ type: "EMAILMESSAGE" });
        _jsCookie2.default.set('JOINED', 'yes', { expires: 2000 });
        console.log("download");
    };
}

function joinUp(data) {
    return function (dispatch) {
        sendDataToSever("joinUp", data);
        dispatch({ type: "POPUPJOINUP" });
        dispatch({ type: "EMAILMESSAGE" });
        _jsCookie2.default.set('JOINED', 'yes', { expires: 2000 });
        console.log("joinded ");
    };
}

function openPopUP(data) {
    return function (dispatch) {
        dispatch({ type: "OPENPOPUP" });
    };
}

function closePopUP(data) {
    return function (dispatch) {
        dispatch({ type: "CLOSEPOPUP" });
    };
}

var sendDataToSever = function sendDataToSever(toFuc, data) {
    fetch('/' + toFuc, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            values: data,
            firstParam: 'yourValue',
            secondParam: 'yourOtherValue' }) });
};
;

var _temp = function () {
    if (typeof __REACT_HOT_LOADER__ === 'undefined') {
        return;
    }

    __REACT_HOT_LOADER__.register(emailMessageBacktoFormContact, "emailMessageBacktoFormContact", "app/redux/2_action/contact.js");

    __REACT_HOT_LOADER__.register(popUpOpen, "popUpOpen", "app/redux/2_action/contact.js");

    __REACT_HOT_LOADER__.register(popUpClose, "popUpClose", "app/redux/2_action/contact.js");

    __REACT_HOT_LOADER__.register(emailMessage, "emailMessage", "app/redux/2_action/contact.js");

    __REACT_HOT_LOADER__.register(asked, "asked", "app/redux/2_action/contact.js");

    __REACT_HOT_LOADER__.register(emailMessageBacktoForm, "emailMessageBacktoForm", "app/redux/2_action/contact.js");

    __REACT_HOT_LOADER__.register(popUpChangeButton, "popUpChangeButton", "app/redux/2_action/contact.js");

    __REACT_HOT_LOADER__.register(downloadRequst, "downloadRequst", "app/redux/2_action/contact.js");

    __REACT_HOT_LOADER__.register(joinUp, "joinUp", "app/redux/2_action/contact.js");

    __REACT_HOT_LOADER__.register(openPopUP, "openPopUP", "app/redux/2_action/contact.js");

    __REACT_HOT_LOADER__.register(closePopUP, "closePopUP", "app/redux/2_action/contact.js");

    __REACT_HOT_LOADER__.register(sendDataToSever, "sendDataToSever", "app/redux/2_action/contact.js");
}();

;