'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.firstLoadCookie = firstLoadCookie;

var _jsCookie = require('js-cookie');

var _jsCookie2 = _interopRequireDefault(_jsCookie);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var onlyOnce = false;
function firstLoadCookie() {
  return function (dispatch) {
    if (!onlyOnce) {
      var join = _jsCookie2.default.get('JOINED');
      if (join == 'yes') {
        dispatch({ type: "POPUPJOINUP" });
      } else {
        var timer = setTimeout(function () {
          dispatch({ type: "POPUPOPEN" });
          console.log("loadPopUP");
        }, 60);
      }
      onlyOnce = true;
    }
  };
}
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(onlyOnce, 'onlyOnce', 'app/redux/2_action/cookies.js');

  __REACT_HOT_LOADER__.register(firstLoadCookie, 'firstLoadCookie', 'app/redux/2_action/cookies.js');
}();

;