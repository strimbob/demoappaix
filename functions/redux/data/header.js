"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var _default = {
  title: "Iris - NODAL SEQUENCER Ableton live plugin [free midi software]",
  titleOg: "Iris - A VISUAL SEQUENCER",
  description: "Iris is a midi nodal sequencer. Pluggin to Ableton Live , Logic , Cubase or any other software that uses Midi. Works out the box with the Novation Launchpad. A modular nodal software sequencer",
  themeColor: "#673AB8",
  keywords: "novation launchpad app , novation launchpad mk2 ,launchpad software ,midi sequencer software free, midi editor mac, visuals music, sequencers, midi sequencer software,sequencer software, Nodel, Nodal",
  image: __dirname + "/static/data/metaLink/1.png",
  url: "https://irislive.org/"
};
exports.default = _default;
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(_default, "default", "app/redux/data/header.js");
}();

;