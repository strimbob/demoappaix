"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var addressBar = exports.addressBar = [{
  name: "iris |",
  link: "/"
}, {
  name: "download |",
  link: "/download"
}, {
  name: "tutorials",
  link: "/tutorials"
}];
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(addressBar, "addressBar", "app/redux/data/addressBar.js");
}();

;