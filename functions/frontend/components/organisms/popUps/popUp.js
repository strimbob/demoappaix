"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _dec, _class;

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require("react-redux");

var _redux = require("redux");

var _reduxForm = require("redux-form");

var _popUpText = require("../../../../redux/data/popUpText.js");

var _contact = require("../../../../redux/2_action/contact.js");

var _MaterialUiFormPopUP = require("../forms/MaterialUiFormPopUP.js");

var _MaterialUiFormPopUP2 = _interopRequireDefault(_MaterialUiFormPopUP);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// import getMuiTheme from 'material-ui/styles/getMuiTheme'
// import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

var check = "";

// const muiTheme = getMuiTheme({}, {
//   radioButton: {
//     borderColor:  "#8C8C8C",
//   },
// });
var rootReducer = (0, _redux.combineReducers)({ form: _reduxForm.reducer });
var storea = (0, _redux.createStore)(rootReducer);

var PopUp = (_dec = (0, _reactRedux.connect)(function (store) {
  return {
    contactForm: store.contactForm
  };
}), _dec(_class = function (_React$Component) {
  _inherits(PopUp, _React$Component);

  function PopUp(props) {
    _classCallCheck(this, PopUp);

    return _possibleConstructorReturn(this, (PopUp.__proto__ || Object.getPrototypeOf(PopUp)).call(this, props));
  }

  _createClass(PopUp, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.props.dispatch((0, _contact.asked)());
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var change = function change(values) {
        console.log(values);
        if (check != values.radioButtonYou) {
          _this2.props.dispatch((0, _contact.popUpChangeButton)(values.radioButtonYou));
          check = values.radioButtonYou;
        }
      };

      var showResults = function showResults(values) {
        _this2.props.dispatch((0, _contact.joinUp)(values));
      };

      if (this.props.contactForm.messageSend) {
        return _react2.default.createElement(
          "div",
          { className: "popUp" },
          _react2.default.createElement(
            "div",
            { className: "popUp__intro" },
            _popUpText.popUp_Text.messageSent,
            _react2.default.createElement("br", null),
            _popUpText.popUp_Text.thankYou
          )
        );
      } else {
        return _react2.default.createElement(
          _reactRedux.Provider,
          { store: storea },
          _react2.default.createElement(
            "div",
            { className: "popUp" },
            _react2.default.createElement(
              "div",
              { className: "popUp__intro" },
              _popUpText.popUp_Text.mainText
            ),
            _react2.default.createElement(_MaterialUiFormPopUP2.default, {
              lastForm: this.props.contactForm.inputTitle,
              onSubmit: showResults,
              onChange: change })
          )
        );
      }
    }
  }]);

  return PopUp;
}(_react2.default.Component)) || _class);
exports.default = PopUp;
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(check, "check", "app/frontend/components/organisms/popUps/popUp.js");

  __REACT_HOT_LOADER__.register(rootReducer, "rootReducer", "app/frontend/components/organisms/popUps/popUp.js");

  __REACT_HOT_LOADER__.register(storea, "storea", "app/frontend/components/organisms/popUps/popUp.js");

  __REACT_HOT_LOADER__.register(PopUp, "PopUp", "app/frontend/components/organisms/popUps/popUp.js");
}();

;