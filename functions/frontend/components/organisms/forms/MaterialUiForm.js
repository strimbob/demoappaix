'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reduxForm = require('redux-form');

var _TextField = require('material-ui/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _RadioButton = require('material-ui/RadioButton');

var _SelectField = require('material-ui/SelectField');

var _SelectField2 = _interopRequireDefault(_SelectField);

var _MenuItem = require('material-ui/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _asyncValidate = require('./asyncValidate');

var _asyncValidate2 = _interopRequireDefault(_asyncValidate);

var _RaisedButton = require('material-ui/RaisedButton');

var _RaisedButton2 = _interopRequireDefault(_RaisedButton);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

var validate = function validate(values) {
  var errors = {};
  var requiredFields = ['firstName', 'email', 'notes'];
  requiredFields.forEach(function (field) {
    if (!values[field]) {
      errors[field] = 'Required';
    }
  });
  if (values.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }
  return errors;
};

var renderTextField = function renderTextField(_ref) {
  var input = _ref.input,
      label = _ref.label,
      _ref$meta = _ref.meta,
      touched = _ref$meta.touched,
      error = _ref$meta.error,
      custom = _objectWithoutProperties(_ref, ['input', 'label', 'meta']);

  return _react2.default.createElement(_TextField2.default, _extends({
    className: 'my',
    hintText: label,
    floatingLabelText: label,
    errorText: touched && error
  }, input, custom));
};

var renderSelectField = function renderSelectField(_ref2) {
  var input = _ref2.input,
      label = _ref2.label,
      _ref2$meta = _ref2.meta,
      touched = _ref2$meta.touched,
      error = _ref2$meta.error,
      children = _ref2.children,
      custom = _objectWithoutProperties(_ref2, ['input', 'label', 'meta', 'children']);

  return _react2.default.createElement(_SelectField2.default, _extends({
    floatingLabelText: label,
    errorText: touched && error
  }, input, {
    onChange: function onChange(event, index, value) {
      return input.onChange(value);
    },
    children: children
  }, custom));
};

var MaterialUiForm = function MaterialUiForm(props) {
  var handleSubmit = props.handleSubmit,
      pristine = props.pristine,
      reset = props.reset,
      submitting = props.submitting;

  return _react2.default.createElement(
    'form',
    { onSubmit: handleSubmit },
    _react2.default.createElement(
      'div',
      null,
      _react2.default.createElement(_reduxForm.Field, {
        className: 'form',
        name: 'firstName',
        component: renderTextField,
        label: 'your name'
      })
    ),
    _react2.default.createElement(
      'div',
      null,
      _react2.default.createElement(_reduxForm.Field, { name: 'email', className: 'form', component: renderTextField, label: 'Email' })
    ),
    _react2.default.createElement(
      'div',
      null,
      _react2.default.createElement(_reduxForm.Field, {
        className: 'form',
        name: 'notes',
        component: renderTextField,
        label: 'message',
        multiLine: true,
        rows: 2
      })
    ),
    _react2.default.createElement(
      'div',
      null,
      _react2.default.createElement(
        'button',
        { className: "subitButton", type: 'submit', disabled: pristine || submitting },
        'send message'
      ),
      _react2.default.createElement(
        'button',
        { className: "clearButton", type: 'button', disabled: pristine || submitting, onClick: reset },
        'Clear Values'
      )
    )
  );
};

var _default = (0, _reduxForm.reduxForm)({
  form: 'MaterialUiForm', // a unique identifier for this form
  validate: validate,
  asyncValidate: _asyncValidate2.default
})(MaterialUiForm);

exports.default = _default;
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(validate, 'validate', 'app/frontend/components/organisms/forms/MaterialUiForm.js');

  __REACT_HOT_LOADER__.register(renderTextField, 'renderTextField', 'app/frontend/components/organisms/forms/MaterialUiForm.js');

  __REACT_HOT_LOADER__.register(renderSelectField, 'renderSelectField', 'app/frontend/components/organisms/forms/MaterialUiForm.js');

  __REACT_HOT_LOADER__.register(MaterialUiForm, 'MaterialUiForm', 'app/frontend/components/organisms/forms/MaterialUiForm.js');

  __REACT_HOT_LOADER__.register(_default, 'default', 'app/frontend/components/organisms/forms/MaterialUiForm.js');
}();

;