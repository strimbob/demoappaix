'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reduxForm = require('redux-form');

var _TextField = require('material-ui/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _RadioButton = require('material-ui/RadioButton');

var _SelectField = require('material-ui/SelectField');

var _SelectField2 = _interopRequireDefault(_SelectField);

var _MenuItem = require('material-ui/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _asyncValidate = require('./asyncValidate');

var _asyncValidate2 = _interopRequireDefault(_asyncValidate);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

var validate = function validate(values) {
  var errors = {};
  var requiredFields = ['firstName', 'email'];
  requiredFields.forEach(function (field) {
    if (!values[field]) {
      errors[field] = 'Required';
    }
  });
  if (values.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }
  return errors;
};

var renderTextField = function renderTextField(_ref) {
  var input = _ref.input,
      label = _ref.label,
      _ref$meta = _ref.meta,
      touched = _ref$meta.touched,
      error = _ref$meta.error,
      custom = _objectWithoutProperties(_ref, ['input', 'label', 'meta']);

  return _react2.default.createElement(_TextField2.default, _extends({
    className: 'my',
    hintText: label,
    floatingLabelText: label,
    errorText: touched && error
  }, input, custom));
};

var renderRadioGroup = function renderRadioGroup(_ref2) {
  var input = _ref2.input,
      rest = _objectWithoutProperties(_ref2, ['input']);

  return _react2.default.createElement(_RadioButton.RadioButtonGroup, _extends({}, input, rest, {
    valueSelected: input.value,
    onChange: function onChange(event, value) {
      return input.onChange(value);
    }
  }));
};

var renderSelectField = function renderSelectField(_ref3) {
  var input = _ref3.input,
      label = _ref3.label,
      _ref3$meta = _ref3.meta,
      touched = _ref3$meta.touched,
      error = _ref3$meta.error,
      children = _ref3.children,
      custom = _objectWithoutProperties(_ref3, ['input', 'label', 'meta', 'children']);

  return _react2.default.createElement(_SelectField2.default, _extends({
    floatingLabelText: label,
    errorText: touched && error
  }, input, {
    onChange: function onChange(event, index, value) {
      return input.onChange(value);
    },
    children: children
  }, custom));
};

var MaterialUiForm = function MaterialUiForm(props) {
  var handleSubmit = props.handleSubmit,
      pristine = props.pristine,
      reset = props.reset,
      submitting = props.submitting;

  console.log(renderTextField.meta);
  console.log(submitting);
  var bottomInput = null;
  // requiredFields(requiredFields);

  // let l = props.lastForm;
  //
  // if((l == "mineral")||(l == "Favourite software")||(l =="one of your Favourite tunes")||(l== " Favourite software")){
  //   bottomInput = <div><Field className="formPOPUP"
  //             name={props.lastForm}
  //             component={renderTextField}
  //             label={props.lastForm}
  //             multiLine={false}
  //             rows={1}/>
  //         </div>
  // }
  // console.log(pristine);
  // pristine = false;
  return _react2.default.createElement(
    'form',
    { onSubmit: handleSubmit },
    _react2.default.createElement(
      'div',
      null,
      _react2.default.createElement(
        'div',
        { className: 'formPOPUPButtons' },
        _react2.default.createElement(
          'div',
          { className: 'formPOPUP__title ' },
          'did you donate ? '
        ),
        _react2.default.createElement(
          _reduxForm.Field,
          { className: 'formPOPUPButtons__formPOPUP', name: 'radioButtonYou', component: renderRadioGroup },
          _react2.default.createElement(_RadioButton.RadioButton, { value: 'yes', label: 'yes' }),
          _react2.default.createElement(_RadioButton.RadioButton, { value: 'no', label: 'no' }),
          _react2.default.createElement(_RadioButton.RadioButton, { value: 'mabeyLater', label: 'mabey later' })
        )
      )
    ),
    _react2.default.createElement(
      'div',
      null,
      _react2.default.createElement(_reduxForm.Field, {
        className: 'formPOPUP',
        name: 'firstName',
        component: renderTextField,
        label: 'name'
      })
    ),
    _react2.default.createElement(
      'div',
      null,
      _react2.default.createElement(_reduxForm.Field, { name: 'email', className: 'formPOPUP', component: renderTextField, label: 'Email' })
    ),
    _react2.default.createElement(
      'div',
      null,
      _react2.default.createElement(
        'div',
        { className: "download" },
        _react2.default.createElement(
          'div',
          { className: "download__buttonSvg" },
          _react2.default.createElement(
            'button',
            { className: "download__button download__button--confirm", type: 'submit', disabled: pristine || submitting },
            _react2.default.createElement(
              'h4',
              null,
              ' download iris '
            )
          )
        )
      ),
      _react2.default.createElement(
        'button',
        { className: "clearButton", type: 'button', disabled: pristine || submitting, onClick: reset },
        'Clear Values'
      )
    )
  );
};

var _default = (0, _reduxForm.reduxForm)({
  form: 'MaterialUiForm', // a unique identifier for this form
  validate: validate,
  asyncValidate: _asyncValidate2.default
})(MaterialUiForm);

exports.default = _default;
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(validate, 'validate', 'app/frontend/components/organisms/forms/doateForm.js');

  __REACT_HOT_LOADER__.register(renderTextField, 'renderTextField', 'app/frontend/components/organisms/forms/doateForm.js');

  __REACT_HOT_LOADER__.register(renderRadioGroup, 'renderRadioGroup', 'app/frontend/components/organisms/forms/doateForm.js');

  __REACT_HOT_LOADER__.register(renderSelectField, 'renderSelectField', 'app/frontend/components/organisms/forms/doateForm.js');

  __REACT_HOT_LOADER__.register(MaterialUiForm, 'MaterialUiForm', 'app/frontend/components/organisms/forms/doateForm.js');

  __REACT_HOT_LOADER__.register(_default, 'default', 'app/frontend/components/organisms/forms/doateForm.js');
}();

;