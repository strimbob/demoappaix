"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _home = require("../frontend/components/pages/home.js");

var _home2 = _interopRequireDefault(_home);

var _appRoot = require("./app-root");

var _appRoot2 = _interopRequireDefault(_appRoot);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var routes = [{ component: _appRoot2.default, routes: [{ path: "/", exact: true, component: _home2.default }] }];
var _default = routes;
exports.default = _default;
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(routes, "routes", "app/router/routes.js");

  __REACT_HOT_LOADER__.register(_default, "default", "app/router/routes.js");
}();

;