"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.daBrain = undefined;

var _path = require("path");

var _path2 = _interopRequireDefault(_path);

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

var _firebaseFunctions = require("firebase-functions");

var functions = _interopRequireWildcard(_firebaseFunctions);

var _expressFuc = require("./expressFuc");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var firebase = require("firebase-admin");

var app = (0, _express2.default)();
var assets = _express2.default.static(_path2.default.join(__dirname, "../public"));
app.use(assets);
app.get("*", _expressFuc.indexRequest);
var daBrain = exports.daBrain = functions.https.onRequest(app);
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(app, "app", "index.js");

  __REACT_HOT_LOADER__.register(assets, "assets", "index.js");

  __REACT_HOT_LOADER__.register(daBrain, "daBrain", "index.js");
}();

;