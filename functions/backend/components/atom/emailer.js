'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.formatEmailSendEmail = undefined;

var _nodemailer = require('nodemailer');

var _nodemailer2 = _interopRequireDefault(_nodemailer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var formatEmailSendEmail = exports.formatEmailSendEmail = function formatEmailSendEmail(data, to, subject) {

    var smtpTransport = _nodemailer2.default.createTransport("SMTP", {
        service: 'Gmail',
        auth: { user: 'megabenlycett@gmail.com',
            pass: '1qaz2wsx0okm9ijn'
        }
    });

    var mailOptions = {
        to: to,
        subject: subject,
        from: "Contact Form Request" + "<" + data.firstName + '>',
        html: "From: " + data.firstName + "<br>" + "User's email: " + data.email + "<br>" + "Message: " + data.notes
    };
    smtpTransport.sendMail(mailOptions, function (err, response) {
        if (err) {
            console.log(err);
        } else {
            console.log("Message sent ");
        }
    });
};
;

var _temp = function () {
    if (typeof __REACT_HOT_LOADER__ === 'undefined') {
        return;
    }

    __REACT_HOT_LOADER__.register(formatEmailSendEmail, 'formatEmailSendEmail', 'app/backend/components/atom/emailer.js');
}();

;