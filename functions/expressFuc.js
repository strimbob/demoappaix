"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.indexRequest = undefined;

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _server = require("react-dom/server");

var _StaticRouter = require("react-router-dom/StaticRouter");

var _StaticRouter2 = _interopRequireDefault(_StaticRouter);

var _reactRouterConfig = require("react-router-config");

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

var _fs = require("fs");

var _fs2 = _interopRequireDefault(_fs);

var _path = require("path");

var _path2 = _interopRequireDefault(_path);

var _routes = require("./router/routes.js");

var _routes2 = _interopRequireDefault(_routes);

var _reactRedux = require("react-redux");

var _store = require("./redux/1_store/store.js");

var _store2 = _interopRequireDefault(_store);

var _reactResponsiveRedux = require("react-responsive-redux");

var _reactHelmet = require("react-helmet");

var _configFireBase = require("./redux/firebase/configFireBase.js");

var _configFireBase2 = _interopRequireDefault(_configFireBase);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var dispatch = _store2.default.dispatch;

var live = false;
var app = (0, _express2.default)();
var _index = _fs2.default.readFileSync(_path2.default.resolve(__dirname, "./public", "nerves.html"), "utf8");
var renderdPages = [{}];
renderdPages.length = 0;
var oneDay = 86400000;
//____________________________________________________
function renderFullPage(html, preloadedState, helmet) {
  var htmlFinal = _index.replace("<!--- ::APP::----->", html);
  htmlFinal = htmlFinal.replace("<!--- ::head__html::----->", helmet.htmlAttributes.toString());
  htmlFinal = htmlFinal.replace("<!--- ::head__title::----->", helmet.title.toString());
  htmlFinal = htmlFinal.replace("<!--- ::head__link::----->", helmet.link.toString());
  htmlFinal = htmlFinal.replace("<!--- ::head__meta::----->", helmet.meta.toString());
  htmlFinal = htmlFinal.replace("<!--- ::head__noscript::----->", helmet.noscript.toString());
  var store_init = "<script> window.__PRELOADED_STATE__ = " + JSON.stringify(preloadedState).replace(/</g, "\\u003c") + " </script>";
  htmlFinal = htmlFinal.replace("<!--- ::store::----->", store_init);
  return htmlFinal;
}
//____________________________________________________
var indexRequest = exports.indexRequest = function indexRequest(request, response) {
  if (process.env.NODE_ENV === "development") {
    response.send(_index);
  } else {
    response.send(_index);
  }
};
//____________________________________________________
var renderPage = function renderPage(request, response) {
  var context = {};
  var mobileDetect = (0, _reactResponsiveRedux.mobileParser)(request);
  var content = (0, _server.renderToString)(_react2.default.createElement(
    _reactRedux.Provider,
    { store: _store2.default },
    _react2.default.createElement(
      "div",
      null,
      _react2.default.createElement(
        _StaticRouter2.default,
        { location: request.url, context: context },
        (0, _reactRouterConfig.renderRoutes)(_routes2.default)
      )
    )
  ));
  var helmet = _reactHelmet.Helmet.renderStatic();
  var preloadedState = _store2.default.getState();
  var newRenderPage = renderFullPage(content, preloadedState, helmet);
  response.send(newRenderPage);
};
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(dispatch, "dispatch", "expressFuc.js");

  __REACT_HOT_LOADER__.register(live, "live", "expressFuc.js");

  __REACT_HOT_LOADER__.register(app, "app", "expressFuc.js");

  __REACT_HOT_LOADER__.register(_index, "_index", "expressFuc.js");

  __REACT_HOT_LOADER__.register(renderdPages, "renderdPages", "expressFuc.js");

  __REACT_HOT_LOADER__.register(oneDay, "oneDay", "expressFuc.js");

  __REACT_HOT_LOADER__.register(renderFullPage, "renderFullPage", "expressFuc.js");

  __REACT_HOT_LOADER__.register(indexRequest, "indexRequest", "expressFuc.js");

  __REACT_HOT_LOADER__.register(renderPage, "renderPage", "expressFuc.js");
}();

;